package com.miehanav.textsizesight;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Saki on 15/2/25.
 */
public class MainActivity extends Activity {
    public static float density;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        density = this.getResources().getDisplayMetrics().density;
        generateAll();
    }
    LinearLayout content;
    private void generateAll(){
        content = (LinearLayout)findViewById(R.id.content);
        content.removeAllViews();
        for (int i=10;i<48;i+=2){
            content.addView(generateText(i));
        }
    }
    private void calculate(){
        for(int i=0;i<content.getChildCount();i++){
            TextView txt= (TextView) content.getChildAt(i);
            txt.setText("real=" + txt.getHeight() + "px|" +txt.getTextSize()+"px "+txt.getTextSize()/density+ "sp Hexlink好连遥控");
        }
    }
    private TextView generateText(float sp){
        TextView txt=new TextView(this);
        txt.setSingleLine();
        txt.setTextSize(sp);
        txt.setTextColor(Color.BLACK);
        txt.setText(sp+"sp Hexlink好连遥控");

        return txt;
    }
    public void ClickShow(View v){
        EditText inputCustom = (EditText) findViewById(R.id.inputCustom);
        try {
            addCustomTextSize(inputCustom.getText().toString());
        }catch(Exception e){
            Toast.makeText(this,"Error Input:"+e.toString(),Toast.LENGTH_LONG);
            inputCustom.setText("");
        }
    }
    private void addCustomTextSize(String textsize){
        int sp = Integer.parseInt(textsize);
        content.addView(generateText(sp),0);
    }
    public void ClickMain(View v){
        calculate();
    }
}
